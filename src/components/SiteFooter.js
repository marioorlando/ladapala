import React, { Component } from 'react';
import { Button, Grid, Col, Form, FormGroup, FormControl, Accordion, Panel } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { isMobile } from 'react-device-detect';
import $ from 'jquery';
import assets from '../services/assets';
import '../styles/SiteFooter.css';

class SiteFooter extends Component {
  constructor() {
    super();
    this.state = {
      defaultActiveKey: 1
    };
  }

  componentWillMount() {
    if (isMobile) {
      this.setState({defaultActiveKey: 0});
    }
  }

  componentDidMount() {
    if (!isMobile) {
      $("a").each(function() {
        var role = $(this).attr("role");
        if(role === 'tab') {
          $(this).contents().unwrap();
        }
      });
    }
  }

  render(){
    return (
      <div className="SiteFooter-container">
        <Grid>

          <Col xs={12} sm={8} className="SiteFooter-email">
            <p>Food Alert - Get our latest food best seller and promotions!</p>
            <Form inline>
              <FormGroup className="SiteFooter-email-input">
                <FormControl
                  type="text"
                  placeholder="Enter your email"
                />
                <FormControl.Feedback />
              </FormGroup>
              <Button className="SiteFooter-email-button" type="submit">
                Subscribe
              </Button>
            </Form>
            <div className="SiteFooter-email-helper">
              <span>{"We'll never share your email address with a third party."}</span>
            </div>
          </Col>

          <Col xs={12} sm={4} className="SiteFooter-share">
            <Col xs={6} className="SiteFooter-share-android">
              <img src={assets.btn_android} alt="btn_android" />
            </Col>
            <Col xs={6} className="SiteFooter-share-ios">
              <img src={assets.btn_ios} alt="btn_ios" />
            </Col>
            <Col xs={12} style={{"marginTop":"10px"}}>
              <Col xs={4} className="SiteFooter-share-text">
                <span>Ikuti kami:</span>
              </Col>
              <Col xs={8} className="SiteFooter-share-socialmedia">
                <a href="https://www.facebook.com/ladapalaID/"><img src={assets.btn_facebook} alt="btn_facebook" /></a>
                <img src={assets.btn_instagram} alt="btn_instagram" />
                <img src={assets.btn_youtube} alt="btn_youtube" />
                <a href="https://twitter.com/LadapalaI"><img src={assets.btn_twitter} alt="btn_twitter" /></a>
                <a href="https://www.linkedin.com/in/ladapala/"><img src={assets.btn_linkedin} alt="btn_linkedin" /></a>
              </Col>
            </Col>
          </Col>

          <Col xs={12} sm={8} className="SiteFooter-link">
            <Col xs={12} sm={3}>
              <Accordion className="SiteFooter-link-accordion" defaultActiveKey={this.state.defaultActiveKey}>
                <Panel className="SiteFooter-link-panel" header="Ladapala" eventKey={1}>
                  <LinkContainer className="SiteFooter-link-href" to="/about"><p>Tentang Kami</p></LinkContainer>
                  <p>Blog</p>
                  <LinkContainer className="SiteFooter-link-href" to="/faq"><p>FAQ</p></LinkContainer>
                  <p>Karir</p>
                </Panel>
              </Accordion>
            </Col>
            <Col xs={12} sm={3}>
              <Accordion className="SiteFooter-link-accordion" defaultActiveKey={this.state.defaultActiveKey}>
                <Panel className="SiteFooter-link-panel" header="LADA (LApar DArurat)" eventKey={1}>
                  <p>Order di Ladapala</p>
                  <p>Cara Order</p>
                  <p>Pembayaran</p>
                  <p>Best Seller</p>
                </Panel>
              </Accordion>
            </Col>
            <Col xs={12} sm={3}>
              <Accordion className="SiteFooter-link-accordion" defaultActiveKey={this.state.defaultActiveKey}>
                <Panel className="SiteFooter-link-panel" header="PALA (PAsar LAuk)" eventKey={1}>
                  <p>Jual di Ladapala</p>
                  <p>Cara Jualan</p>
                  <p>Gold Kitchen</p>
                  <p>Pasang Iklan</p>
                </Panel>
              </Accordion>
            </Col>
            <Col xs={12} sm={3}>
              <Accordion className="SiteFooter-link-accordion" defaultActiveKey={this.state.defaultActiveKey}>
                <Panel className="SiteFooter-link-panel" header="Bantuan" eventKey={1}>
                  <p>Syarat dan Ketentuan</p>
                  <p>Kebijakan Privasi</p>
                  <p>Hubungi Kami</p>
                  <p>Panduan Keamanan</p>
                </Panel>
              </Accordion>
            </Col>
          </Col>

          <Col xs={12} sm={4} className="SiteFooter-logo">
            <Col xs={12} sm={8} className="SiteFooter-logo-text">
              <p>@2017 Ladapala.com</p>
              <p>All rights reserved</p>
            </Col>
            <Col xsHidden={true} sm={4} className="SiteFooter-logo-img">
              <img src={assets.favicon} alt="favicon" />
            </Col>
          </Col>

        </Grid>
      </div>
    )
  }
}

export default SiteFooter;
