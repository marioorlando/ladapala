import React, { Component } from 'react';
import {Grid, Col} from 'react-bootstrap';
import assets from '../../services/assets';
import '../../styles/About/About.css';

class About extends Component {
  render(){
    return (
      <div className="About-container">
        <Grid>
          <Col xs={12} sm={3} className="About-leftbar">
            <h3><b>TENTANG</b></h3>
            <img src={assets.logo_clean} alt="logo_clean" />
            <img src={assets.mobile} style={{"marginTop":"20px"}} alt="mobile" />
          </Col>
          <Col xs={12} sm={9} className="About-midrightbar">
            <Col xs={12} sm={8} className="About-midbar">
              <div className="About-content">
                <h2><b>Kebutuhan Makanan Dalam Genggaman Anda</b></h2>
                <br/>
                <p>Berawal dari keinginan untuk menciptakan suatu sistem yang ingin membuktikan bahwa semua orang berhak untuk mendapatkan makanan yang layak, makanan yang bergizi, terlepas dari siapa latar belakangnya, sesibuk apakah kegiatannya, maupun status sosialnya, LADAPALA kini hadir sebagai life-style mobile platform yang menyediakan berbagai layanan lengkap seputar kuliner.</p>
                <p>LADAPALA adalah sebuah perusahaan teknologi yang memberikan wadah bagi siapapun untuk saling berbagi. Di LADAPALA, Anda dapat menjual atau mebagikan hasil karya masakan Anda, sayur dan buah yang berlebih, maupun industri kuliner rumahan yang Anda miliki. Banyak dari beberapa orang memiliki keahlian untuk memasak suatu kuliner khas tertentu dan ingin mengkomersilkannya atau sekedar membagikannya, di sinilah mereka dapat menyalurkan hobi sambil bersosialisasi dengan lingkungan.</p>
                <p>Sebagai user, LADAPALA mengerti bahwa sebagian dari kita, entah terbatas oleh karena kesibukan pekerjaan sehari-hari sehingga sulit meluangkan waktu untuk menghadirkan makanan bergizi bagi dirinya maupun keluarganya. Dengan LADAPALA, kami ingin mewujudkan keinginan mereka bahwa kita semua berhak untuk mendapatkan makanan yang sama, makanan yang layak dan bergizi.</p>
              </div>
            </Col>
            <Col xs={12} sm={4} className="About-rightbar">
              <img src={assets.lapapoint} alt="lapapoint" />
            </Col>
          </Col>
        </Grid>
      </div>
    )
  }
}

export default About;
