import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import assets from '../services/assets';
import '../styles/SiteNav.css';

class SiteNav extends Component {
  render(){
    return (
      <Navbar className="SiteNav-navbar" fixedTop={true}>
        <Navbar.Header>
          <Navbar.Brand>
            <LinkContainer to="/">
              <img className="SiteNav-logo" src={assets.logo} alt="logo"></img>
            </LinkContainer>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight className="SiteNav-menubar">
            <LinkContainer to="/about">
              <NavItem>TENTANG KAMI</NavItem>
            </LinkContainer>
            <NavItem>FITUR</NavItem>
            <LinkContainer to="/faq">
              <NavItem>FAQ</NavItem>
            </LinkContainer>
            <NavItem className="SiteNav-join">GABUNG</NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default SiteNav;
