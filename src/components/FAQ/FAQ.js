import React, { Component } from 'react';
import {Grid, Col} from 'react-bootstrap';
import '../../styles/FAQ/FAQ.css';

class FAQ extends Component {
  render(){
    return (
      <div className="FAQ-container">
        <Grid>
          <Col xs={12}>
            <p className="FAQ-questions">Q : Apakah itu LADAPALA?</p>
            <p>A : Ladapala adalah sebuah gaya hidup kuliner zaman modern. Di mana kita dapat saling berbagi hasil masakan rumahan kita, baik dijual maupun dibagikan secara gratis. Aplikasi ini sangat berguna bagi pecinta kuliner dan penggemar masak-memasak.</p>
            <p className="FAQ-questions">Q : Bagaimana aplikasi ini bekerja?</p>
            <p>A : Aplikasi ini bekerja dengan menghubungkan Lada (Lapar Darurat) dan Pala (Pasar Lauk) Lada dapat menemukan makanan apa yang tersedia di sekitar mereka, memesan makanan, dan mengambil makanan tersebut saat sudah siap. Pala dapat membagikan foto hasil kreasi kuliner mereka, menjualnya, atau bahkan membagikannya secara gratis.</p>
            <p className="FAQ-questions">Q : Dapatkah saya sekaligus menjadi Lada dan Pala?</p>
            <p>A : Bisa. Dengan memilih pilihan Lada atau Pala saat membuat profile Anda.</p>
            <p className="FAQ-questions">Q : Bagaimana saya melakukan pembayaran?</p>
            <p>A: Pembayaran dapat dilakukan pada saat transaksi (Cash on delivery/COD)</p>
            <p className="FAQ-questions">Q : Bagaimana saya memberikan harga pada masakan saya?</p>
            <p>A : Sepenuhnya menjadi kebijakan Anda. Setiap rupiah yang Anda jual, sepenuhnya milik Anda. Berusahalah menjual dengan harga kompetitif untuk mendapatkan banyak pembeli (Lada).</p>
            <p className="FAQ-questions">Q : Saya mempunyai beberapa alergi makanan, bagaimana saya dapat mengklarifikasinya dengan Pasar Lauk (Pala)?</p>
            <p>A : Pala akan menuliskan bahan-bahan apa saja yang terkandung dalam masakan mereka. Untuk lebih memastikannya, Anda dapat mengirimkan SMS atau meneleponnya.</p>
          </Col>
        </Grid>
      </div>
    )
  }
}

export default FAQ;
