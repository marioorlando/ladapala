import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';
import { Button, Grid, Col, Glyphicon } from 'react-bootstrap';

import assets from '../../services/assets';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import '../../styles/Home/Home.css';

class Home extends Component {
  render() {
    var settings = {
      showStatus: false,
      showThumbs: false,
      autoPlay: true,
      infiniteLoop: true
    };

    return (
      <div className="Home-container">
        <Carousel {...settings}>
          <div>
            <Button className="Home-slideshow1-btn">INFO LANJUT</Button>
            <img src={assets.slideshow_1} alt="slideshow1" />
          </div>
          <div>
            <Button className="Home-slideshow2-btn">DAFTAR SEKARANG</Button>
            <img src={assets.slideshow_2} alt="slideshow2" />
          </div>
          <div>
            <Button className="Home-slideshow3-btn">DOWNLOAD APPS</Button>
            <img src={assets.slideshow_3} alt="slideshow3" />
          </div>
        </Carousel>
        <Grid>
          <div className="Home-barcontainer">
            <Col xs={12} sm={6} className="Home-leftbar">
              <p style={{"color":"#f48400"}}><b>TENTANG LADAPALA</b></p>
              <h3>KEBUTUHAN MAKANAN DALAM GENGGAMAN ANDA</h3>
              <p>Berawal dari keinginan untuk menciptakan suatu sistem yang ingin membuktikan bahwa semua orang berhak untuk mendapatkan makanan yang layak, makanan yang bergizi, terlepas dari siapa latar belakangnya, sesibuk apakah kegiatannya, maupun status sosialnya, LADAPALA kini hadir sebagai life-style mobile platform yang menyediakan berbagai layanan lengkap seputar kuliner.</p>
              <a href="/#/about" style={{"textDecoration":"none"}}>
                <p style={{"color":"#f48400"}}><b>SELANJUTNYA</b>  <Glyphicon glyph="chevron-right"/></p>
              </a>
            </Col>
            <Col xs={12} sm={6} className="Home-rightbar">
              <img src={assets.video} alt="video" />
            </Col>
          </div>
        </Grid>
      </div>
    )
  }
}

export default Home;
