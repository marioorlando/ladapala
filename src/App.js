import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import SiteNav from './components/SiteNav';
import Home from './components/Home/Home';
import About from './components/About/About';
import FAQ from './components/FAQ/FAQ';
import SiteFooter from './components/SiteFooter';
import './App.css';

class App extends Component {
  componentDidUpdate() {
    window.scrollTo(0,0);
  }

  render() {
    return (
      <div className="App">
        <SiteNav />
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/about' component={About}/>
          <Route path='/faq' component={FAQ}/>
        </Switch>
        <SiteFooter />
      </div>
    );
  }
}

export default App;
