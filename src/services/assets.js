var favicon = process.env.PUBLIC_URL + '/favicon.png';
var logo = process.env.PUBLIC_URL + '/images/ladapala.png';
var logo_clean = process.env.PUBLIC_URL + '/images/ladapala-clean.png';
var slideshow_1 = process.env.PUBLIC_URL + '/images/slideshow_1.png';
var slideshow_2 = process.env.PUBLIC_URL + '/images/slideshow_2.png';
var slideshow_3 = process.env.PUBLIC_URL + '/images/slideshow_3.png';
var lapapoint = process.env.PUBLIC_URL + '/images/lapapoint.png';
var mobile = process.env.PUBLIC_URL + '/images/mobile.png';
var video = process.env.PUBLIC_URL + '/images/video.jpg';
var btn_android = process.env.PUBLIC_URL + '/images/btn-android.png';
var btn_ios = process.env.PUBLIC_URL + '/images/btn-ios.png';
var btn_facebook = process.env.PUBLIC_URL + '/images/btn-facebook.png';
var btn_instagram = process.env.PUBLIC_URL + '/images/btn-instagram.png';
var btn_twitter = process.env.PUBLIC_URL + '/images/btn-twitter.png';
var btn_youtube = process.env.PUBLIC_URL + '/images/btn-youtube.png';
var btn_linkedin = process.env.PUBLIC_URL + '/images/btn-linkedin.png';

export default {
  logo,
  logo_clean,
  slideshow_1,
  slideshow_2,
  slideshow_3,
  lapapoint,
  mobile,
  video,
  btn_android,
  btn_ios,
  btn_facebook,
  btn_instagram,
  btn_twitter,
  btn_youtube,
  btn_linkedin,
  favicon
}
